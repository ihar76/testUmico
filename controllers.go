package main

import (
	"fmt"
	"math/rand"
	"net/http"
	"testTask/throtlling"
)

type controller struct{ *throtlling.Pool }

// sample for send message
func (c controller) SendMessage(w http.ResponseWriter, r *http.Request) {
	sess := GetSession(w, r)
	userIdInterface := sess.Get("userid")
	if userIdInterface == nil {
		fmt.Fprintf(w, "Need login")
		return
	}
	user := userIdInterface.(string)

	if ok, _ := c.CheckLimit(THROTTLE_MESSAGES, user); ok {
		c.AddThrottle(THROTTLE_MESSAGES, user)
		fmt.Fprintf(w, "Message send!")
	}
	fmt.Fprintf(w, "Error send message! Limit expired")
}

// sample for transactions
func (c controller) SendTransactions(w http.ResponseWriter, r *http.Request) {
	sess := GetSession(w, r)
	userIdInterface := sess.Get("userid")
	if userIdInterface == nil {
		fmt.Fprintf(w, "Need login")
		return
	}
	user := userIdInterface.(string)
	if ok, _ := c.CheckLimit(THROTTLE_TRANSACTIONS, user); ok {
		success := rand.Intn(1)
		if success == 0 {
			fmt.Fprintf(w, "Transaction fails!")
			c.AddThrottle(THROTTLE_TRANSACTIONS, user)
		}
	} else {
		fmt.Fprintf(w, "Error send transactions! Limit expired")
	}
}

// sample for create accounts
func (c controller) CreateAccounts(w http.ResponseWriter, r *http.Request) {
	user := r.RemoteAddr
	if ok, _ := c.CheckLimit(THROTTLE_ACCOUNTS, user); ok {
		c.AddThrottle(THROTTLE_ACCOUNTS, user)
		fmt.Fprintf(w, "Account created!")
	}
	fmt.Fprintf(w, "Error create account from your IP! Limit expires")
	fmt.Fprintf(w, "Hello World!")
}
