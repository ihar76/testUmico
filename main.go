package main

import (
	"net/http"
	"testTask/throtlling"
	"time"
)

const (
	THROTTLE_MESSAGES     = "messages"
	THROTTLE_TRANSACTIONS = "transactions"
	THROTTLE_ACCOUNTS     = "accounts"
)

func main() {
	c := controller{throtlling.GetPool()}
	c.NewThrottle(THROTTLE_MESSAGES, time.Second, 1)
	c.NewThrottle(THROTTLE_TRANSACTIONS, 24*time.Hour, 3)
	c.NewThrottle(THROTTLE_ACCOUNTS, 24*time.Hour, 20)
	http.HandleFunc("/message", c.SendMessage)
	http.HandleFunc("/transactions", c.SendTransactions)
	http.HandleFunc("/createaccount", c.CreateAccounts)
	http.HandleFunc("/login", login) // not work, only for example and create sessions
	http.ListenAndServe(":80", nil)
}
