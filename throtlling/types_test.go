package throtlling

import (
	"strconv"
	"testing"
	"time"
)

const (
	LimitCnt = 6
)

func TestPool(t *testing.T) {
	t.Parallel()
	pool := GetPool()
	pool.NewThrottle("test1", 5*time.Second, LimitCnt)

	t.Log("Given the need to test Pool's behavior at different time.")
	{
		testID := 1
		t.Logf("\tTest %d:\tcheck and create limit.", testID)

		{
			for i := 0; i <= LimitCnt; i++ {
				ok, _ := pool.CheckLimit("test1", "client1")

				if !ok {
					if i <= LimitCnt {
						t.Logf("Success limit Throttle for client (false)")
					} else {
						t.Fatalf("Error limit Throttle for client (false)")
					}
				} else {
					pool.AddThrottle("test1", "client1")
					t.Logf("Success limit Throttle for client (true)")
				}
			}
		}
	}

}

func TestPool2(t *testing.T) {
	t.Parallel()
	pool := GetPool()
	pool.NewThrottle("test1", 5*time.Second, LimitCnt)

	t.Log("Given the need to test Pool's behavior at different time.")
	{

		t.Logf("\tTest %d:\tcheck and create limit.", 2)

		{
			for i := 0; i <= LimitCnt; i++ {
				ok, _ := pool.CheckLimit("test1", "client3")

				if !ok {
					if i <= LimitCnt {
						t.Logf("Success limit Throttle for client (false)")
					} else {
						t.Fatalf("Error limit Throttle for client (false)")
					}
				} else {
					pool.AddThrottle("test1", "client3")
					t.Logf("Success limit Throttle for client (true)")
				}
			}
		}

	}
}

func BenchmarkSample(b *testing.B) {
	pool := GetPool()
	pool.NewThrottle("test1", 5*time.Second, LimitCnt)
	for i := 0; i < b.N; i++ {

		clientName := "client-" + strconv.Itoa(i)
		{
			{
				for i := 0; i <= LimitCnt; i++ {
					ok, _ := pool.CheckLimit("test1", clientName)

					if !ok {
						if i > LimitCnt {
							b.Fatalf("Error limit Throttle for client (false)")
						}
					} else {
						pool.AddThrottle("test1", clientName)
					}
				}
			}

		}
	}
}
