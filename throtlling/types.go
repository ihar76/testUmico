package throtlling

import (
	"errors"
	"sync"
	"time"
)

type (
	Pool struct {
		*sync.RWMutex
		Throttles map[string]*Throtlle
	}

	Throtlle struct {
		Time     map[string][]time.Time
		Limit    time.Duration
		LimitCnt int
	}
)

func GetPool() *Pool {
	p := Pool{}
	p.RWMutex = new(sync.RWMutex)
	p.Throttles = make(map[string]*Throtlle)

	return &p
}

// create new trottle in pool
func (p *Pool) NewThrottle(name string, limit time.Duration, limitCnt int) error {
	p.Lock()
	defer p.Unlock()

	if _, ok := p.Throttles[name]; !ok {
		p.Throttles[name] = &Throtlle{Limit: limit, LimitCnt: limitCnt, Time: map[string][]time.Time{}}
	} else {
		return errors.New("Throttle with name " + name + " exist")
	}
	return nil
}

func (p *Pool) AddThrottle(name, client string) error {
	p.Lock()
	defer p.Unlock()
	// check throttlename
	if thr, ok := p.Throttles[name]; ok {

		// check if times of client exists
		if _, ok := thr.Time[client]; !ok {
			thr.Time[client] = []time.Time{}
		}

		// create new event for client
		thr.Time[client] = append(thr.Time[client], time.Now())
	} else {
		return errors.New("Throttle with name " + name + " exist")
	}

	return nil
}

func (p *Pool) CheckLimit(name, client string) (bool, error) {
	p.Lock()
	defer p.Unlock()

	// check for throttles exist
	if _, ok := p.Throttles[name]; !ok {
		return false, errors.New("Throttle with name " + name + " isn't exist")
	}

	if _, ok := p.Throttles[name].Time[client]; !ok {
		p.Throttles[name].Time[client] = []time.Time{}
		return true, nil
	}

	i := 0
	// check all time for expired by duration
	// if duration is not expired return result by count times
	for {
		if i > len(p.Throttles[name].Time[client]) {
			break
		}

		// if time is actual
		if time.Now().Sub(p.Throttles[name].Time[client][i]) < p.Throttles[name].Limit {
			// need only actual times
			p.Throttles[name].Time[client] = p.Throttles[name].Time[client][i:]
			return len(p.Throttles[name].Time[client]) < p.Throttles[name].LimitCnt, nil
		}
		i++
	}

	return true, nil
}
